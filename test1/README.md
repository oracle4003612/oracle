# 实验1：SQL语句的执行计划分析与优化指导

- 学号：123，姓名：ZWD，班级：123

## 实验目的

分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

## 实验过程

### 权限分配过程

```oracle
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
```

![](./img/image1.png)

```oralce
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr; 
```

![](./img/image2.png)

查询方法一：

![](./img/image3.png)

![](./img/image4.png)

![](./img/image5.png)

查询方法二：

```oracle
SQL> set autotrace on
SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
FROM hr.departments d,hr.employees e
WHERE d.department_id = e.department_id
GROUP BY d.department_name
  6  HAVING d.department_name in ('IT','Sales');
```

DEPARTMENT_NAME                部门总人数   平均工资
------------------------------ ---------- ----------
IT					5	5760
Sales				       34 8955.88235


执行计划
----------------------------------------------------------
Plan hash value: 2128232041

--------------------------------------------------------------------------------
--------------

| Id  | Operation		       | Name	     | Rows  | Bytes | Cost (%CP
U)| Time     |

--------------------------------------------------------------------------------
--------------

|   0 | SELECT STATEMENT	       |	     |	   1 |	  23 |	   7  (2
9)| 00:00:01 |

|*  1 |  FILTER 		       |	     |	     |	     |
  |	     |

|   2 |   HASH GROUP BY 	       |	     |	   1 |	  23 |	   7  (2
9)| 00:00:01 |

|   3 |    MERGE JOIN		       |	     |	 106 |	2438 |	   6  (1
7)| 00:00:01 |

|   4 |     TABLE ACCESS BY INDEX ROWID| DEPARTMENTS |	  27 |	 432 |	   2   (
0)| 00:00:01 |

|   5 |      INDEX FULL SCAN	       | DEPT_ID_PK  |	  27 |	     |	   1   (
0)| 00:00:01 |

|*  6 |     SORT JOIN		       |	     |	 107 |	 749 |	   4  (2
5)| 00:00:01 |

|   7 |      TABLE ACCESS FULL	       | EMPLOYEES   |	 107 |	 749 |	   3   (
0)| 00:00:01 |

--------------------------------------------------------------------------------
--------------

![](./img/image6.png)







## 结果分析

可以看出两种查询方式的结果是相同的，说明两种查询方式都是正确的，但是第一种查询方式用时比第二种方式长，第二种查询方式更优。

![](./img/image7.png)

![](./img/image8.png)
