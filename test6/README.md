# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 
学号：202010414407 姓名：李凤 班级：20级软工四班
- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

# 数据库设计方案：
## 表空间设计方案
1. xitong表空间：存放Oracle的系统表、索引、存储过程等系统级别的对象。
2. shuju表空间：存放用户数据表、索引等对象。
3. suoyin表空间：存放数据表的索引对象。
## 表设计方案 
1. 商品信息表（goods_info）：存储商品的基本信息，包括商品ID、名称、价格、库存等字段。
2. 订单信息表（order_info）：存储订单的基本信息，包括订单ID、用户ID、商品ID、数量、总价、下单时间等字段。
3. 用户信息表（user_info）：存储用户的基本信息，包括用户ID、用户名、密码、性别、年龄等字段。
4. 支付信息表（payment_info）：存储用户的支付信息，包括支付ID、用户ID、订单ID、支付方式、支付金额、支付时间等字段。

## 数据量设计方案
为了模拟真实的商品销售系统，我们至少需要10万条数据。其中，商品信息表、订单信息表和用户信息表的数据量应该是比较大的，可以根据实际情况进行调整。例如，可以将商品信息表的数据量设置为2万条，订单信息表的数据量设置为5万条，用户信息表的数据量设置为3万条。
## 权限及用户分配方案 
为了保证数据的安全性和完整性，我们需要分配不同的用户和权限。在本系统中，我们可以创建两个用户：一个是管理员用户，另一个是普通用户。管理员用户拥有所有表的读写权限，可以对所有数据进行增、删、改、查；普通用户只拥有商品信息表和订单信息表的读写权限，不能对用户信息表和支付信息表进行操作。
## 备份方案设计 
为了保证数据的安全性和恢复性，我们需要设计一套数据库备份方案。具体方案如下：
1. 定期进行全量备份：每周进行一次全量备份，将所有表的数据备份到磁盘上，以便在数据出现问题时进行恢复。
2. 定期进行增量备份：每天进行一次增量备份，将当天发生变更的数据备份到磁盘上，以便在数据出现问题时进行局部恢复。
3. 将备份数据存储到远程服务器：将备份数据复制到远程服务器上，以防止本地硬件故障导致数据丢失。
4. 定期测试备份数据的可用性：定期测试备份数据的可用性，以确保在数据出现问题时可以快速恢复数据。
5. 使用日志文件进行恢复：在数据出现问题时，可以使用Oracle的日志文件进行恢复，以避免数据丢失或者损坏。
 # 具体实现过程
 1. 表空间设计 
 ```
 -- 创建xitong表空间
CREATE TABLESPACE xitong 
DATAFILE '/home/oracle/database/tbs_order_detail_index' 
SIZE 100M 
AUTOEXTEND ON 
NEXT 10M 
MAXSIZE UNLIMITED 
EXTENT MANAGEMENT LOCAL;

-- 创建shuju表空间
CREATE TABLESPACE shuju
DATAFILE '/home/oracle/database/tbs_prod_index' 
SIZE 500M 
AUTOEXTEND ON 
NEXT 50M 
MAXSIZE UNLIMITED 
EXTENT MANAGEMENT LOCAL;

-- 创建suoyin表空间
CREATE TABLESPACE suoyin 
DATAFILE '/home/oracle/database/tbs_orders_index' 
SIZE 200M 
AUTOEXTEND ON 
NEXT 20M 
MAXSIZE UNLIMITED 
EXTENT MANAGEMENT LOCAL;
 ```
![a1](./a1.png)
2. 表设计 
```

--商品信息表（goods_info）
CREATE TABLE goods_info (
    goods_id NUMBER(10) PRIMARY KEY,
    goods_name VARCHAR2(50) NOT NULL,
    goods_price NUMBER(10,2) NOT NULL,
    goods_stock NUMBER(10) NOT NULL
) TABLESPACE shuju;

--订单信息表（order_info）
CREATE TABLE order_info (
    order_id NUMBER(10) PRIMARY KEY,
    user_id NUMBER(10) NOT NULL,
    goods_id NUMBER(10) NOT NULL,
    order_num NUMBER(10) NOT NULL,
    order_amount NUMBER(10,2) NOT NULL,
    order_time DATE NOT NULL
) TABLESPACE shuju;

--用户信息表（user_info)
CREATE TABLE user_info (
    user_id NUMBER(10) PRIMARY KEY,
    user_name VARCHAR2(50) NOT NULL,
    password VARCHAR2(50) NOT NULL,
    gender VARCHAR2(10) NOT NULL,
    age NUMBER(3) NOT NULL
) TABLESPACE shuju;
-- 支付信息表（payment_info）
CREATE TABLE payment_info (
    payment_id NUMBER(10) PRIMARY KEY,
    user_id NUMBER(10) NOT NULL,
    order_id NUMBER(10) NOT NULL,
    payment_method VARCHAR2(50) NOT NULL,
    payment_amount NUMBER(10,2) NOT NULL,
    payment_time DATE NOT NULL
) TABLESPACE shuju;

```
![a2](./a2.png)
3. 权限及用户分配 
- 管理员用户
在Oracle数据库中创建一个名为"admin"的用户，并赋予该用户一些特定的权限和角色。具体来说：

1. CREATE USER admin IDENTIFIED BY admin123; 创建了一个名为"admin"的用户，并设置该用户的密码为"admin123"。这个用户将被用于连接到数据库并执行相应的操作。

2. GRANT CONNECT, RESOURCE, DBA TO admin; 授予了"admin"用户三个角色的权限，包括：

    - CONNECT：允许用户连接到数据库实例。
    - RESOURCE：允许用户创建表、索引、序列、程序包等对象。
    - DBA：允许用户执行数据库管理员权限范围内的所有操作，包括备份和恢复数据库、创建和修改用户等。
3. GRANT ALL PRIVILEGES TO admin; 授予了"admin"用户所有特权，使其可以执行任何操作，包括对其他用户授予和回收权限等。
```
CREATE USER admin IDENTIFIED BY admin123;
GRANT CONNECT, RESOURCE, DBA TO admin;
GRANT ALL PRIVILEGES TO admin;
```
![a3](./a3.png)
- 普通用户
在Oracle数据库中创建一个名为"user1"的用户，并赋予该用户一些特定的权限和角色。具体来说：

1. CREATE USER user1 IDENTIFIED BY user123; 创建了一个名为"user1"的用户，并设置该用户的密码为"user123"。这个用户将被用于连接到数据库并执行相应的操作。

2. GRANT CONNECT, RESOURCE TO user1; 授予了"user1"用户两个角色的权限，包括：

    - CONNECT：允许用户连接到数据库实例。
    - RESOURCE：允许用户创建表、索引、序列、程序包等对象。
3. GRANT SELECT, INSERT, UPDATE, DELETE ON goods_info TO user1; 授予了"user1"用户对"goods_info"表的SELECT、INSERT、UPDATE和DELETE权限。这意味着该用户可以查询、插入、更新和删除"goods_info"表中的数据。

4. GRANT SELECT, INSERT, UPDATE, DELETE ON order_info TO user1; 授予了"user1"用户对"order_info"表的SELECT、INSERT、UPDATE和DELETE权限。这意味着该用户可以查询、插入、更新和删除"order_info"表中的数据。
```
CREATE USER user1 IDENTIFIED BY user123;
GRANT CONNECT, RESOURCE TO user1;
GRANT SELECT, INSERT, UPDATE, DELETE ON goods_info TO user1;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_info TO user1;
```
![a4](./a4.png)
4. 插入数据
向四个表中插入测试数据
- 向goods_info表中插入50,000条记录，每条记录包含商品ID、商品名称、商品价格和商品库存量。
- 向user_info表中插入20,000条记录，每条记录包含用户ID、用户名、密码、性别和年龄等信息。
- 向order_info表中插入50,000条记录，每条记录包含订单ID、用户ID、商品ID、订单数量、订单金额和订单时间等信息。其中，商品ID和用户ID是通过随机数生成的，订单数量和订单金额根据商品价格和随机数计算得出，订单时间是当前系统时间减去随机数生成的时间间隔。
- 向payment_info表中插入50,000条记录，每条记录包含支付ID、用户ID、订单ID、支付方式、支付金额和支付时间等信息。其中，用户ID和订单ID是通过随机数生成的，支付方式是从4种选项中随机选择的，支付金额等于订单金额，支付时间是当前系统时间减去随机数生成的时间间隔。
```
DECLARE
  v_goods_id NUMBER(10);
  v_goods_name VARCHAR2(50);
  v_goods_price NUMBER(10,2);
  v_goods_stock NUMBER(10);

  v_order_id NUMBER(10);
  v_user_id NUMBER(10);
  v_user_name VARCHAR2(50); -- 添加v_user_name变量声明
  v_order_num NUMBER(10);
  v_order_amount NUMBER(10,2);
  v_order_time DATE;

  v_password VARCHAR2(50);
  v_gender VARCHAR2(10);
  v_age NUMBER(3);

  v_payment_id NUMBER(10);
  v_payment_method VARCHAR2(50);
  v_payment_amount NUMBER(10,2);
  v_payment_time DATE;
BEGIN
  -- Insert data into goods_info table
  FOR i IN 1..50000 LOOP
    v_goods_id := i;
    v_goods_name := 'Goods_' || TO_CHAR(i);
    v_goods_price := ROUND(DBMS_RANDOM.VALUE(1, 100), 2);
    v_goods_stock := TRUNC(DBMS_RANDOM.VALUE(1, 1000));
    INSERT INTO goods_info VALUES (v_goods_id, v_goods_name, v_goods_price, v_goods_stock);
  END LOOP;
  COMMIT;

  -- Insert data into user_info table
  FOR i IN 1..20000 LOOP
    v_user_id := i;
    v_user_name := 'User_' || TO_CHAR(i);
    v_password := 'Password_' || TO_CHAR(i);
    v_gender := CASE TRUNC(DBMS_RANDOM.VALUE(1, 3)) WHEN 1 THEN 'Male' ELSE 'Female' END;
    v_age := TRUNC(DBMS_RANDOM.VALUE(18, 100));
    INSERT INTO user_info VALUES (v_user_id, v_user_name, v_password, v_gender, v_age);
  END LOOP;
  COMMIT;

  -- Insert data into order_info table
  FOR i IN 1..50000 LOOP
    v_order_id := i;
    v_user_id := TRUNC(DBMS_RANDOM.VALUE(1, 20001));
    v_goods_id := TRUNC(DBMS_RANDOM.VALUE(1, 50001));
    v_order_num := TRUNC(DBMS_RANDOM.VALUE(1, 10));
    SELECT goods_price INTO v_goods_price FROM goods_info WHERE goods_id = v_goods_id;
    v_order_amount := v_goods_price * v_order_num;
    v_order_time := SYSDATE - DBMS_RANDOM.VALUE(0, 365);
    INSERT INTO order_info VALUES (v_order_id, v_user_id, v_goods_id,v_order_num, v_order_amount, v_order_time);
  END LOOP;
  COMMIT;

  -- Insert data into payment_info table
  FOR i IN 1..50000 LOOP
    v_payment_id := i;
    v_user_id := TRUNC(DBMS_RANDOM.VALUE(1, 20001));
    v_order_id := TRUNC(DBMS_RANDOM.VALUE(1, 50001));
    v_payment_method := CASE TRUNC(DBMS_RANDOM.VALUE(1, 4))
                          WHEN 1 THEN 'Credit Card'
                          WHEN 2 THEN 'Debit Card'
                          WHEN 3 THEN 'PayPal'
                          ELSE 'Cash'
                        END;
    SELECT order_amount INTO v_order_amount FROM order_info WHERE order_id = v_order_id;
    v_payment_amount := v_order_amount;
    v_payment_time := SYSDATE - DBMS_RANDOM.VALUE(0, 365);
    INSERT INTO payment_info VALUES (v_payment_id, v_user_id, v_order_id, v_payment_method, v_payment_amount, v_payment_time);
  END LOOP;
  COMMIT;
END;
/
```
![a5](./a5.png)
goods_info表
![a6](./a6.png)
order_info表
![a7](./a7.png)
user_info表
![a8](./a8.png)
payment_info表
![a9](./a9.png)
5. 备份方案设计 

  (1) 全量备份:

    ```
    expdp SYSTEM/password FULL=Y DIRECTORY=backup_dir DUMPFILE=full_backup.dmp LOGFILE=full_backup.log;

    ```

  (2) 增量备份

    ```

    expdp SYSTEM/password DIRECTORY=backup_dir DUMPFILE=incremental_backup.dmp LOGFILE=incremental_backup.log TABLES=goods_info, order_info;

    ```
  (3) 备份数据存储到远程服务器：
  可以使用scp命令将备份数据复制到远程服务器上

    ```

    scp full_backup.dmp user@remote_server:/backup_dir/
    ```
  (4) 测试备份数据的可用性

    ```
    impdp SYSTEM/password FULL=Y DIRECTORY=backup_dir DUMPFILE=full_backup.dmp LOGFILE=full_restore.log;
    ```

  (5) 使用日志文件进行恢复
  
    ```
    RECOVER DATABASE;
    ```
6. 程序包及存储过程和函数设计
- 程序包（sales_pack）
```
CREATE OR REPLACEPACKAGE sales_pack AS
  PROCEDURE place_order(p_user_id IN NUMBER, p_goods_id IN NUMBER, p_order_num IN NUMBER);
  FUNCTION calculate_order_amount(p_goods_id IN NUMBER, p_order_num IN NUMBER) RETURN NUMBER;
END sales_pack;
/
```
- 存储过程（place_order）
```
CREATE OR REPLACE PROCEDURE sales_pack.place_order(p_user_id IN NUMBER, p_goods_id IN NUMBER, p_order_num IN NUMBER) AS
  v_order_id NUMBER;
  v_order_amount NUMBER;
  BEGIN
    v_order_id := SEQ_ORDER_ID.NEXTVAL;
    v_order_amount := sales_pack.calculate_order_amount(p_goods_id, p_order_num);
    INSERT INTO order_info(order_id, user_id, goods_id, order_num, order_amount, order_time)
    VALUES(v_order_id, p_user_id, p_goods_id, p_order_num, v_order_amount, SYSDATE);
    UPDATE goods_info SET goods_stock = goods_stock - p_order_num WHERE goods_id = p_goods_id;
  END place_order;
/
```
- 函数（calculate_order_amount
```
CREATE OR REPLACE FUNCTION sales_pack.calculate_order_amount(p_goods_id IN NUMBER, p_order_num IN NUMBER) RETURN NUMBER AS
  v_goods_price NUMBER;
  v_order_amount NUMBER;
  BEGIN
    SELECT goods_price INTO v_goods_price FROM goods_info WHERE goods_id = p_goods_id;
    v_order_amount := v_goods_price * p_order_numEND calculate_order_amount;
/
```



