-- 创建SYSTEM表空间
CREATE TABLESPACE xitong
    DATAFILE '/home/oracle/database/tbs_order_detail_index' SIZE 100M
    AUTOEXTEND ON NEXT 10M MAXSIZE UNLIMITED
    EXTENT MANAGEMENT LOCAL;

-- 创建DATA表空间
CREATE TABLESPACE shuju
    DATAFILE '/home/oracle/database/tbs_prod_index' SIZE 500M
    AUTOEXTEND ON NEXT 50M MAXSIZE UNLIMITED
    EXTENT MANAGEMENT LOCAL;

-- 创建INDEX表空间
CREATE TABLESPACE suoyin
    DATAFILE '/home/oracle/database/tbs_orders_index' SIZE 200M
    AUTOEXTEND ON NEXT 20M MAXSIZE UNLIMITED
    EXTENT MANAGEMENT LOCAL;

--商品信息表（goods_info）
CREATE TABLE goods_info (
    goods_id    NUMBER(10) PRIMARY KEY,
    goods_name  VARCHAR2(50) NOT NULL,
    goods_price NUMBER(10, 2) NOT NULL,
    goods_stock NUMBER(10) NOT NULL
)
TABLESPACE shuju;

--订单信息表（order_info）
CREATE TABLE order_info (
    order_id     NUMBER(10) PRIMARY KEY,
    user_id      NUMBER(10) NOT NULL,
    goods_id     NUMBER(10) NOT NULL,
    order_num    NUMBER(10) NOT NULL,
    order_amount NUMBER(10, 2) NOT NULL,
    order_time   DATE NOT NULL
)
TABLESPACE shuju;

--用户信息表（user_info)
CREATE TABLE user_info (
    user_id   NUMBER(10) PRIMARY KEY,
    user_name VARCHAR2(50) NOT NULL,
    password  VARCHAR2(50) NOT NULL,
    gender    VARCHAR2(10) NOT NULL,
    age       NUMBER(3) NOT NULL
)
TABLESPACE shuju;
-- 支付信息表（payment_info）
CREATE TABLE payment_info (
    payment_id     NUMBER(10) PRIMARY KEY,
    user_id        NUMBER(10) NOT NULL,
    order_id       NUMBER(10) NOT NULL,
    payment_method VARCHAR2(50) NOT NULL,
    payment_amount NUMBER(10, 2) NOT NULL,
    payment_time   DATE NOT NULL
)
TABLESPACE shuju;

CREATE USER admin IDENTIFIED BY admin123;

GRANT connect, resource, dba TO admin;

GRANT ALL PRIVILEGES TO admin;

CREATE USER user1 IDENTIFIED BY user123;

GRANT connect, resource TO user1;

GRANT SELECT, INSERT, UPDATE, DELETE ON goods_info TO user1;

GRANT SELECT, INSERT, UPDATE, DELETE ON order_info TO user1;

DECLARE
    v_goods_id       NUMBER(10);
    v_goods_name     VARCHAR2(50);
    v_goods_price    NUMBER(10, 2);
    v_goods_stock    NUMBER(10);
    v_order_id       NUMBER(10);
    v_user_id        NUMBER(10);
    v_user_name      VARCHAR2(50); -- 添加v_user_name变量声明
    v_order_num      NUMBER(10);
    v_order_amount   NUMBER(10, 2);
    v_order_time     DATE;
    v_password       VARCHAR2(50);
    v_gender         VARCHAR2(10);
    v_age            NUMBER(3);
    v_payment_id     NUMBER(10);
    v_payment_method VARCHAR2(50);
    v_payment_amount NUMBER(10, 2);
    v_payment_time   DATE;
BEGIN
  -- Insert data into goods_info table
    FOR i IN 1..50000 LOOP
        v_goods_id := i;
        v_goods_name := 'Goods_' || to_char(i);
        v_goods_price := round(dbms_random.value(1, 100), 2);
        v_goods_stock := trunc(dbms_random.value(1, 1000));
        INSERT INTO goods_info VALUES (
            v_goods_id,
            v_goods_name,
            v_goods_price,
            v_goods_stock
        );

    END LOOP;

    COMMIT;

  -- Insert data into user_info table
    FOR i IN 1..20000 LOOP
        v_user_id := i;
        v_user_name := 'User_' || to_char(i);
        v_password := 'Password_' || to_char(i);
        v_gender :=
            CASE trunc(dbms_random.value(1, 3))
                WHEN 1 THEN
                    'Male'
                ELSE 'Female'
            END;

        v_age := trunc(dbms_random.value(18, 100));
        INSERT INTO user_info VALUES (
            v_user_id,
            v_user_name,
            v_password,
            v_gender,
            v_age
        );

    END LOOP;

    COMMIT;

  -- Insert data into order_info table
    FOR i IN 1..50000 LOOP
        v_order_id := i;
        v_user_id := trunc(dbms_random.value(1, 20001));
        v_goods_id := trunc(dbms_random.value(1, 50001));
        v_order_num := trunc(dbms_random.value(1, 10));
        SELECT
            goods_price
        INTO v_goods_price
        FROM
            goods_info
        WHERE
            goods_id = v_goods_id;

        v_order_amount := v_goods_price * v_order_num;
        v_order_time := sysdate - dbms_random.value(0, 365);
        INSERT INTO order_info VALUES (
            v_order_id,
            v_user_id,
            v_goods_id,
            v_order_num,
            v_order_amount,
            v_order_time
        );

    END LOOP;

    COMMIT;

  -- Insert data into payment_info table
    FOR i IN 1..50000 LOOP
        v_payment_id := i;
        v_user_id := trunc(dbms_random.value(1, 20001));
        v_order_id := trunc(dbms_random.value(1, 50001));
        v_payment_method :=
            CASE trunc(dbms_random.value(1, 4))
                WHEN 1 THEN
                    'Credit Card'
                WHEN 2 THEN
                    'Debit Card'
                WHEN 3 THEN
                    'PayPal'
                ELSE 'Cash'
            END;

        SELECT
            order_amount
        INTO v_order_amount
        FROM
            order_info
        WHERE
            order_id = v_order_id;

        v_payment_amount := v_order_amount;
        v_payment_time := sysdate - dbms_random.value(0, 365);
        INSERT INTO payment_info VALUES (
            v_payment_id,
            v_user_id,
            v_order_id,
            v_payment_method,
            v_payment_amount,
            v_payment_time
        );

    END LOOP;

    COMMIT;
END;
/

CREATE OR REPLACE PACKAGE sales_pack AS
  PROCEDURE place_order(p_user_id IN NUMBER, p_goods_id IN NUMBER, p_order_num IN NUMBER);
  FUNCTION calculate_order_amount(p_goods_id IN NUMBER, p_order_num IN NUMBER) RETURN NUMBER;
END sales_pack;
/
CREATE OR REPLACE PROCEDURE sales_pack.place_order(p_user_id IN NUMBER, p_goods_id IN NUMBER, p_order_num IN NUMBER) AS
  v_order_id NUMBER;
  v_order_amount NUMBER;
  BEGIN
    v_order_id := SEQ_ORDER_ID.NEXTVAL;
    v_order_amount := sales_pack.calculate_order_amount(p_goods_id, p_order_num);
    INSERT INTO order_info(order_id, user_id, goods_id, order_num, order_amount, order_time)
    VALUES(v_order_id, p_user_id, p_goods_id, p_order_num, v_order_amount, SYSDATE);
    UPDATE goods_info SET goods_stock = goods_stock - p_order_num WHERE goods_id = p_goods_id;
  END place_order;
/


