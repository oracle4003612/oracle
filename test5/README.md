# 实验五：
- 学号：202010414407 姓名：李凤 班级：20级软工四班
# 实验目的
- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。
# 实验内容
- 以hr用户登录 
    1. 创建一个包(Package)，包名是MyPack。
    2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
    3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：
```
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID

```

# 实验步骤
第一步：创建MyPack包
```
CREATE OR REPLACE PACKAGE MyPack AS
  FUNCTION Get_SalaryAmount(departmentID NUMBER) RETURN NUMBER;
  PROCEDURE GET_EMPLOYEES(employeeID NUMBER);
END MyPack;

```
![a1](./a1.png)
第二步：实现MgPack
包含了两个子程序：一个函数 Get_SalaryAmount 和一个过程 GET_EMPLOYEES。
1. 函数 Get_SalaryAmount 接受一个参数 V_DEPARTMENT_ID，返回一个 NUMBER 类型的值。函数实现的功能是查询员工表中指定部门的薪资总额，并将结果返回给调用者。具体实现过程是使用 SELECT 语句计算指定部门的 salary 总额，并将结果存储在变量 N 中，最后将 N 返回给调用者。
2. 过程 GET_EMPLOYEES 接受一个参数 V_EMPLOYEE_ID，没有返回值。过程实现的功能是递归查询指定员工及其所有下属、子下属的员工信息，并将结果输出到控制台。具体实现过程是使用游标和递归查询语句查询指定员工及其下属、子下属的员工信息，然后将查询结果按照递归层次输出到控制台。
```
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/

```
![a2](./a2.png)

第三步：测试
 - 函数Get_SalaryAmount()测试
 ```
 select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
 ```
 ![a3](./a3.png)
 - 过程Get_Employees()测试
 ```
 set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/

 ```
![a4](./a4.png)

# 总结
本次实验学习了PL/SQL语言结构，包括了PL/SQL变量和常量的声明和使用方法，以及包、过程和函数的用法。

在实验中，我们首先以hr用户登录Oracle数据库，然后创建一个名为MyPack的包，包含了两个子程序：一个函数Get_SalaryAmount和一个过程GET_EMPLOYEES。Get_SalaryAmount函数接收一个参数部门ID，通过查询员工表，统计每个部门的salary工资总额。GET_EMPLOYEES过程接收一个参数员工ID，在过程中使用游标，通过递归查询某个员工及其所有下属、子下属员工。

实验过程中需要注意的是，在实现Get_SalaryAmount函数中，需要使用SELECT语句计算指定部门的salary总额，并将结果存储在变量N中。在实现GET_EMPLOYEES过程中，需要使用游标和递归查询语句查询指定员工及其下属、子下属的员工信息，然后将查询结果按照递归层次输出到控制台。此外，在测试过程中，需要注意SQL语句的正确性和参数的传递方式。

总之，本次实验通过实践让我们更深入地理解了PL/SQL语言结构和包、过程、函数的用法。在实际开发中，PL/SQL可以帮助我们更高效地处理大量数据和完成复杂的业务逻辑。同时，我们也需要注意PL/SQL程序的性能优化和安全性问题，以保证程序的可靠性和稳定性。

