# 实验四：PL/SQL语言打印杨辉三角
- 学号：202010414407 姓名：李凤 班级：20软工四班
# 实验目的
掌握Oracle PL/SQL语言以及存储过程的编写。
# 实验内容
- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriangle的SQL语句。
# 实验步骤

第一步:创建YHTriangle存储过程。
我们定义了一个名为 YHTriangle 的存储过程，该过程接受一个整数参数 N，用于指定需要打印的杨辉三角的行数。在存储过程中，我们将源代码中的变量和类型定义转换为存储过程中的参数和变量，并将输出语句替换为 DBMS_OUTPUT.PUT_LINE 等输出到控制台的语句。
```
CREATE OR REPLACE PROCEDURE YHTriangle(N IN INTEGER) AS
    TYPE t_number IS VARRAY (100) OF INTEGER NOT NULL; --数组
    i INTEGER;
    j INTEGER;
    spaces VARCHAR2(30) :='   '; --三个空格，用于打印时分隔数字
    rowArray t_number := t_number();
BEGIN
    DBMS_OUTPUT.PUT_LINE('1'); --先打印第1行
    DBMS_OUTPUT.PUT(RPAD(1,9,' '));--先打印第2行
    DBMS_OUTPUT.PUT(RPAD(1,9,' '));--打印第一个1
    DBMS_OUTPUT.PUT_LINE(''); --打印换行
    --初始化数组数据
    FOR i IN 1 .. N LOOP
        rowArray.EXTEND;
    END LOOP;
    rowArray(1):=1;
    rowArray(2):=1;    
    FOR i IN 3 .. N --打印每行，从第3行起
    LOOP
        rowArray(i):=1;    
        j:=i-1;
        --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
        --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        WHILE j>1 LOOP
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        END LOOP;
        --打印第i行
        FOR j IN 1 .. i LOOP
            DBMS_OUTPUT.PUT(RPAD(rowArray(j),9,' '));--打印第一个1
        END LOOP;
        DBMS_OUTPUT.PUT_LINE(''); --打印换行
    END LOOP;
END YHTriangle;
/
```
![a1](./a1.png)

第二步：调用该存储过程并输出杨辉三角
```
BEGIN
    YHTriangle(9); --打印9行杨辉三角
END;
```
![a2](./a2.png)
在执行以上语句后，程序会调用 YHTriangle 存储过程，并将 9 作为参数传入，从而输出 9 行的杨辉三角。
第三步：在 SQL Developer工具中启用 DBMS_OUTPUT 输出。
``` 
SET SERVEROUTPUT ON;
```
![a3](./a3.png)

# 结论
根据实验要求，编写了一个 Oracle PL/SQL 存储过程 YHTriangle，用于打印指定行数的杨辉三角。该存储过程使用了一个 VARRAY 类型的数组来存储杨辉三角的每一行数字，并使用了循环语句来计算每一行的数字，并使用 DBMS_OUTPUT.PUT_LINE 函数将结果输出到控制台。

在实验中，我们通过创建 YHTriangle 存储过程并传入不同的参数值，可以打印出不同行数的杨辉三角。通过测试，我们可以得出以下结论：

1. Oracle PL/SQL 语言是一种强大的编程语言，可以用于编写存储过程、触发器、函数等数据库对象，能够实现丰富的数据库操作和业务逻辑。
2. 存储过程是一种在数据库中预先编译和存储的程序，可以在需要时被调用执行。存储过程可以提高数据库操作的效率，避免重复编写相同的代码，并且可以加强数据的安全性。
3. 在 PL/SQL 中，使用 VARRAY 类型的数组可以方便地存储和操作一组数据，可以通过 EXTEND 函数动态扩展数组的大小，也可以通过下标访问数组中的元素。
4. 使用 DBMS_OUTPUT.PUT_LINE 函数可以将输出结果输出到控制台，方便调试和测试。
5. 杨辉三角是一种有趣的数学结构，它可以展示出二项式系数的规律，也可以用于解决一些简单的数学问题，如计算组合数等。在实验中，我们编写的存储过程可以打印出指定行数的杨辉三角，展示出其规律和美丽之处。

