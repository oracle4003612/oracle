# 实验三: 创建分区表
- 学号：202010414407 姓名：李凤 班级：20软工四班
# 实验目的
掌握分区表的创建方法，掌握各种分区方式的使用场景
# 实验内容
- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。
# 实验步骤

第一步：创建orders表,该表包含以下列：

  - order_id: 订单编号，数据类型为NUMBER，长度为9，不允许为空，且为主键。
  - customer_name: 客户名称，数据类型为VARCHAR2，长度为40，不允许为空。
  - customer_tel: 客户电话，数据类型为VARCHAR2，长度为40，不允许为空。
  - order_date: 订单日期，数据类型为DATE，不允许为空。
  - employee_id: 员工编号，数据类型为NUMBER，长度为6，不允许为空。
  - discount: 折扣金额，数据类型为NUMBER，长度为8，小数点后保留2位，缺省值为0。
  - trade_receivable: 应收账款金额，数据类型为NUMBER，长度为8，小数点后保留2位，缺省值为0。
- 该表使用了分区技术，按照订单日期进行分区，分为三个分区：
  - PARTITION_BEFORE_2016: 存储订单日期在2016年之前的数据。
  - PARTITION_BEFORE_2020: 存储订单日期在2016年到2020年之间的数据。
  - PARTITION_BEFORE_2021: 存储订单日期在2020年到2021年之间的数据。
  - 该表还预留了一个名为partition_before_2022的分区，用于存储2021年到2022年之间的数据，该分区的创建使用了ALTER TABLE语句。

表的存储设置如下：
  -  数据表空间：USERS。
  - PCTFREE：表示块内留有未使用空间的百分比，默认为10%。
 - INITRANS：指定用于管理块并发的初始事务槽数，默认为1。
 - 存储设置：使用默认缓冲池，不启用压缩和并行。
```
  CREATE TABLE orders 
  (
   order_id NUMBER(9, 0) NOT NULL
   , customer_name VARCHAR2(40 BYTE) NOT NULL 
   , customer_tel VARCHAR2(40 BYTE) NOT NULL 
   , order_date DATE NOT NULL 
   , employee_id NUMBER(6, 0) NOT NULL 
   , discount NUMBER(8, 2) DEFAULT 0 
   , trade_receivable NUMBER(8, 2) DEFAULT 0 
   , CONSTRAINT ORDERS_PK PRIMARY KEY 
    (
      ORDER_ID 
    )
  ) 
  TABLESPACE USERS 
  PCTFREE 10 INITRANS 1 
  STORAGE (   BUFFER_POOL DEFAULT ) 
  NOCOMPRESS NOPARALLEL 
  
  PARTITION BY RANGE (order_date) 
  (
   PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
   TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
   'NLS_CALENDAR=GREGORIAN')) 
   NOLOGGING
   TABLESPACE USERS
   PCTFREE 10 
   INITRANS 1 
   STORAGE 
  ( 
   INITIAL 8388608 
   NEXT 1048576 
   MINEXTENTS 1 
   MAXEXTENTS UNLIMITED 
   BUFFER_POOL DEFAULT 
  ) 
  NOCOMPRESS NO INMEMORY  
  , PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
  TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
  'NLS_CALENDAR=GREGORIAN')) 
  NOLOGGING
  TABLESPACE USERS
  , PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
  TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
  'NLS_CALENDAR=GREGORIAN')) 
  NOLOGGING 
  TABLESPACE USERS
  );
  --以后再逐年增加新年份的分区
  ALTER TABLE orders ADD PARTITION partition_before_2022
  VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
  TABLESPACE USERS;
  
  ```
![a1](./a1.png)

第二步：创建rder_details表，该表包含了以下列：
 - id: 订单详情编号，数据类型为NUMBER，长度为9，不允许为空，且为主键。
- order_id: 订单编号，数据类型为NUMBER，长度为10，不允许为空，且为外键，关联到"orders"表的"order_id"列。
- product_id: 产品编号，数据类型为VARCHAR2，长度为40，不允许为空。
- product_num: 产品数量，数据类型为NUMBER，长度为8，小数点后保留2位，不允许为空。
- product_price: 产品单价，数据类型为NUMBER，长度为8，小数点后保留2位，不允许为空。

该表使用了引用分区技术，即使用了外键约束来实现分区，分区键为"order_id"列，即订单编号。这意味着，当"orders"表中的某个订单被删除时，该订单对应的"order_details"表中的所有相关记录也将被删除。

该表的存储设置与"orders"表相同。

该表还定义了两个约束：

- ORDER_DETAILS_PK：主键约束，用于保证 "order_details"表中的每条记录都具有唯一的ID值。
- order_details_fk1：外键约束，用于保证"order_details"表中的每个订单详情记录都与"orders"表中的某个订单相关联。

```
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);

```

![a2](./a2.png)

第三步：创建序列SEQ1
```
CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
```
![a3](./a3.png)

在order表中插入数据的脚本
```
declare 
   i integer;
   y integer;
   m integer;
   d integer;
   str varchar2(100);
BEGIN  
  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<100 loop
    i := i+1;
    --在这里改变y,m,d
    m:=m+1;
    if m>12 then
        m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) 
      values(SEQ1.nextval,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;

```


# 结论

本次实验的主要目的是掌握分区表的创建方法和各种分区方式的使用场景。在实验中，我们使用了Oracle数据库，通过创建两张表（orders和order_details）并对其进行分区，来学习分区表的使用方法。具体实验内容如下：

1. 创建订单表（orders）和订单详表（order_details），并设置主外键关联。同时，为orders表的customer_name列创建B-Tree索引。
2. 创建两个序列，分别用于orders表的order_id列和order_details表的id列，以自动为其生成唯一的ID值。
3. 对orders表按照订单日期（order_date）进行范围分区，以实现数据按时间分区存储。
4. 对order_details表使用引用分区，将其与orders表关联。
5. 插入数据，并确保数据能够平均分布到各个分区中。
6. 编写联合查询语句，查询orders表和order_details表的数据，并分析其执行计划。
7. 进行分区与不分区的对比实验，以比较其性能差异

总的来说，本次实验通过实际操作，让我们掌握了分区表的创建和使用方法。在实验中，我们了解了分区表的优点，例如可以提高查询效率、降低维护成本、提高数据安全性等.

在实验中，我们还学习了如何为分区表创建索引、设置主外键关联、使用序列生成ID值等常见操作。在数据插入方面，我们也学会了如何将数据平均分布到各个分区中，以避免某些分区数据过多或过少的情况。

最后，我们还学会了如何使用联合查询语句查询分区表的数据，并分析查询语句的执行计划。通过分析执行计划，我们可以评估查询语句的性能并优化其执行效率。

在实验结束时，我们还进行了分区与不分区的对比实验，以比较其性能差异。实验结果表明，在大规模数据情况下，分区表的查询性能要显著优于非分区表，因为分区表可以将数据按照某种规则分散存储，从而减小了查询范围，提高了查询效率。同时，分区表也能更好地支持数据维护和管理，能够更加灵活地进行数据备份、恢复、迁移等操作，提高了系统的可靠性和可维护性。

总的来说，通过本次实验，我们掌握了分区表的创建和使用方法，并了解了其优点和适用场景。在实际应用中，我们可以根据具体需求和数据特点，选择合适的分区方式和方案，以提高系统的性能和可靠性。


